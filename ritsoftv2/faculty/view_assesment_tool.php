<?php 
    include("includes/header.php");
    include("includes/sidenav.php");
    include("includes/connection3.php");
    
    $st=$_SESSION['fid'];
        
?>
<div id="page-wrapper">

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-8">
            <h1 class="page-header">View Assessment Tool</h1>
        </div>
    </div>
    

    <?php
        $assesment_tool_sel = mysql_query("select * from accreditation_assesment_tool inner join subject_allocation on accreditation_assesment_tool.subjectid = subject_allocation.subjectid where fid='$st'",$con);

        $at_count = mysql_num_rows($assesment_tool_sel);

        if($at_count == 0)
        {
    ?>
    <div class="text-center">
            <h3>No Assessment Tool Availabe</h3>

    </div>
    <?php
        }
        else{
    ?>

    <div class="card">
        <div class="card-body">
            <table class="table table-success table-bordered table-striped">
                <thead style="font-size:20px;">
                    <tr>
                        <th style="text-align: center;" > Class </th>
                        <th style="text-align: center;" > Subject </th>
                        <th style="text-align: center;" > Assessment Tool Category</th>
                        <th style="text-align: center;" > Name</th>
                        <th style="text-align: center;" > No of Questions</th>
                        <th style="text-align: center;" > Weightage</th>
                        <th style="text-align: center;" > Action </th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                        // $dept_name = $_GET['dept_name'];
                        
                        $assesment_tool_sel = mysql_query("select * from accreditation_assesment_tool inner join subject_allocation on accreditation_assesment_tool.subjectid = subject_allocation.subjectid inner join accreditation_assessment_tool_category on accreditation_assesment_tool.category_id=accreditation_assessment_tool_category.ass_category_id where fid='$st'",$con);
                        while($res_at = mysql_fetch_array($assesment_tool_sel))
                        {
                        
                    ?>
                        <tr style="font-size:18px;"> 
                            <td style="text-align: center; "><?php echo $res_at['class_id']; ?> </td>
                            <td style="text-align: center; "><?php echo $res_at['subjectid']; ?> </td>
                            <td style="text-align: center;"><?php echo $res_at['category_name']; ?></td>
                            <td style="text-align: center;"><?php echo $res_at['name']; ?></td>
                            <td style="text-align: center;"><?php echo $res_at['no_of_questions']; ?></td>
                            <td style="text-align: center;"><?php echo $res_at['weightage']; ?></td>
                            <td style="text-align: center;">
                                <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#<?php echo $res_at['po_id'] ?>ModalCenter">
                                    <i class="fa fa-pencil" aria-hidden="true" style="font-size: 18px"></i>
                                </button>
                                <a href="accreditation/delete_po.php?po_id=<?php echo $res_at['po_id']; ?>&dept_name=<?php echo $dept_name; ?>">
                                    <button type="button" class="btn btn-danger">
                                        <i class="fa fa-trash" aria-hidden="true" style="font-size: 18px"></i>
                                    </button>
                                </a>
                                
                            </td>

                        </tr>
                       
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            
        </div>
        <?php 
            }			
        ?>
    </div>
    
                
<?php include("includes/footer.php");?>
