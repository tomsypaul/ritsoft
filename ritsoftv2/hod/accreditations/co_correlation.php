<?php
    include("../../connection.php");
    include("../includes/header.php");
    // include("../includes/sidenav.php");


    $facultyId = $_SESSION['fid'];
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Course Outcome View</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script src="jquery.min.js"></script>
    <script>
        function fetch_co(sub_id)
        {
            $.ajax({
                type : 'post',
                url : 'fetch_co.php',
                data : {
                    subjectid : sub_id
                },
                success: function (response)
                {
                    document.getElementById("co_code").innerHTML=response;
                }
            });
        }
    </script>
	
</head>
<body>
    
    
    <div class="row 1">
        <div class="col-md-3">

        </div>
    <div class="col-md-8">
        <div class="panel panel-danger" style="margin-top:80px;">
            <div class="panel-heading">
                <h3 class="panel-title">CO-PO Correlation</h3>
            </div>
            <form name="co" method="post" action="#">
                <div class="panel-body">
                    <fieldset class="form-group">
                        <label for="course">Subject Id</label>
                        <select class="form-control" id="course_id" name="course_id" onchange="fetch_co(this.value)" style="margin-top:10px;">
					        <option selected disabled>Select</option>
        			    <?php
                            $sql_subject = mysql_query("select subjectid from subject_allocation where fid ='$facultyId'",$con);
                            while($res_subject=mysql_fetch_array($sql_subject))
                            {
                                $sub_id=$res_subject['subjectid'];
                             
                                $sql_sub_details = mysql_query("select * from subject_class where subjectid ='$sub_id'",$con);
                                while($res_sub_details = mysql_fetch_array($sql_sub_details))
                                {
                        ?>
                                <option value="<?php echo $res_sub_details['subjectid']; ?>">
                                    <?php echo $res_sub_details['subjectid'].' '.$res_sub_details['subject_title']; ?>
                                </option>
                        <?php
                                }
                            } 
                        ?>    
                        </select>
                    </fieldset>
                </div>
                <div class="panel-body">
                    <fieldset class="form-group">
                        <label for="course">CO Code</label>
                        <select class="form-control" id="co_code" name="co_code" style="margin-top:10px;">
					    
                        </select>
                    </fieldset>
                </div>
		        <div class="button-panel">
		            <input type="submit" class="btn btn-info" title="View CO" name="view" id="view" value="View CO" style="margin-left:20px; margin-bottom:25px;">
		        </div>
                <?php
                    
                    if(isset($_POST['view'])){
                        $co_code = $_POST['co_code'];
                        $sql_copo_corelation = mysql_query("select * from copo_corelation where co_code ='$co_code'",$con);
                ?>
			        <table class="table table-bordered table-hover">
				        <thead>
				        	<tr>
				        		<th style="text-align: center;" > CO-PO </th>
				        		<th style="text-align: center;" > PO1 </th>
				        		<th style="text-align: center;" > PO2 </th>
				        		<th style="text-align: center;" > PO3 </th>
				        		<th style="text-align: center;" > PO4 </th>
				        		<th style="text-align: center;" > PO5 </th>
				        		<th style="text-align: center;" > PO6 </th>
				        		<th style="text-align: center;" > PO7 </th>
				        		<th style="text-align: center;" > PO8 </th>
				        		<th style="text-align: center;" > PO9 </th>
				        		<th style="text-align: center;" > PO10 </th>
				        		<th style="text-align: center;" > PO11 </th>
				        		<th style="text-align: center;" > PO12 </th>
				        	</tr>
				        </thead>
				        <tbody>
                            <?php
                                while($res_copo = mysql_fetch_array($sql_copo_corelation))
                                {
                            ?>
				        	    <tr> 
		                	        <td style="text-align: center;"><?php echo $res_copo['co_code']; ?> </td>
		                	        <td style="text-align: center;"><?php echo $res_copo['po1']; ?></td>
		                	        <td style="text-align: center;"><?php echo $res_copo['po2']; ?></td>
		                	        <td style="text-align: center;"><?php echo $res_copo['po3']; ?></td>
		                	        <td style="text-align: center;"><?php echo $res_copo['po4']; ?></td>
		                	        <td style="text-align: center;"><?php echo $res_copo['po5']; ?></td>
		                	        <td style="text-align: center;"><?php echo $res_copo['po6']; ?></td>
		                	        <td style="text-align: center;"><?php echo $res_copo['po7']; ?></td>
		                	        <td style="text-align: center;"><?php echo $res_copo['po8']; ?></td>
		                	        <td style="text-align: center;"><?php echo $res_copo['po9']; ?></td>
		                	        <td style="text-align: center;"><?php echo $res_copo['po10']; ?></td>
		                	        <td style="text-align: center;"><?php echo $res_copo['po11']; ?></td>
		                	        <td style="text-align: center;"><?php echo $res_copo['po12']; ?></td>
				        	    </tr>
                            <?php } ?>    
			            </tbody>
			        </table>
                <?php } ?>    
			</div>
        </div>
	    <a href="http://localhost:8080/myproject/faculty/dash_home.php"> Back to home</a>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="jquery.min.js"></script>
</body>
</html>

<?php
    include("../includes/footer.php");
?>