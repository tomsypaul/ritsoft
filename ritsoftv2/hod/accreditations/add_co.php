<?php

    include("../../connection.php");
	include("../includes/header.php");
    // include("sidenav.php");
global $i;
global $insert;
?>

<!--<script src="jquery.js"></script>-->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Course Outcome Add</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="js/jquery.min.js"></script>
	<style>
	#selectpo {
		color: red;
	}
	</style>

<script>
        function enableSelect1(){
            var check1 = document.getElementById('check1');
            var select1 = document.getElementById('d1');
            var hidden1 = document.getElementById('hidden1');
            if(check1.checked == true){
                select1.disabled = false;
                hidden1.disabled =  true;
            }
            else{
                select1.disabled = true;
            }
        }
        function enableSelect2(){
            var check2 = document.getElementById('check2');
            var select2 = document.getElementById('d2');
            var hidden2 = document.getElementById('hidden2');
            if(check2.checked == true){
                select2.disabled = false;
                hidden2.disabled =  true;
            }
            else{
                select2.disabled = true;
            }
        }
        function enableSelect3(){
            var check3 = document.getElementById('check3');
            var select3 = document.getElementById('d3');
            var hidden3 = document.getElementById('hidden3');
            if(check3.checked == true){
                select3.disabled = false;
                hidden3.disabled =  true;
            }
            else{
                select3.disabled = true;
            }
        }
        function enableSelect4(){
            var check4 = document.getElementById('check4');
            var select4 = document.getElementById('d4');
            var hidden4 = document.getElementById('hidden4');
            if(check4.checked == true){
                select4.disabled = false;
                hidden4.disabled =  true;
            }
            else{
                select4.disabled = true;
            }
        }
        function enableSelect5(){
            var check5 = document.getElementById('check5');
            var select5 = document.getElementById('d5');
            var hidden5 = document.getElementById('hidden5');
            if(check5.checked == true){
                select5.disabled = false;
                hidden5.disabled =  true;
            }
            else{
                select5.disabled = true;
            }
        }
        function enableSelect6(){
            var check6 = document.getElementById('check6');
            var select6 = document.getElementById('d6');
            var hidden6 = document.getElementById('hidden6');
            if(check6.checked == true){
                select6.disabled = false;
                hidden6.disabled =  true;
            }
            else{
                select6.disabled = true;
            }
        }
        function enableSelect7(){
            var check7 = document.getElementById('check7');
            var select7 = document.getElementById('d7');
            var hidden7 = document.getElementById('hidden7');
            if(check7.checked == true){
                select7.disabled = false;
                hidden7.disabled =  true;
            }
            else{
                select7.disabled = true;
            }
        }
        function enableSelect8(){
            var check8 = document.getElementById('check8');
            var select8 = document.getElementById('d8');
            var hidden8 = document.getElementById('hidden8');
            if(check8.checked == true){
                select8.disabled = false;
                hidden8.disabled =  true;
            }
            else{
                select8.disabled = true;
            }
        }
        function enableSelect9(){
            var check9 = document.getElementById('check9');
            var select9 = document.getElementById('d9');
            var hidden9 = document.getElementById('hidden9');
            if(check9.checked == true){
                select9.disabled = false;
                hidden9.disabled =  true;
            }
            else{
                select9.disabled = true;
            }
        }
        function enableSelect10(){
            var check10 = document.getElementById('check10');
            var select10 = document.getElementById('d10');
            var hidden10 = document.getElementById('hidden10');
            if(check10.checked == true){
                select10.disabled = false;
                hidden10.disabled =  true;
            }
            else{
                select10.disabled = true;
            }
        }
        function enableSelect11(){
            var check11 = document.getElementById('check11');
            var select11 = document.getElementById('d11');
            var hidden11 = document.getElementById('hidden11');
            if(check11.checked == true){
                select11.disabled = false;
                hidden11.disabled =  true;
            }
            else{
                select11.disabled = true;
            }
        }
        function enableSelect12(){
            var check12 = document.getElementById('check12');
            var select12 = document.getElementById('d12');
            var hidden12 = document.getElementById('hidden12');
            if(check12.checked == true){
                select12.disabled = false;
                hidden12.disabled =  true;
            }
            else{
                select12.disabled = true;
            }
        }

    </script>
  </head>
  <body>
  

   <div class="row 1">
      <div class="col-md-3">

      </div>
      <div class="col-md-8">
		
		<form name="co_add" action="" method="post">
			<!--<div class="split left">
			<div class="centered">
			-->
        <div class="panel panel-danger" style="margin-top:80px;">
          <div class="panel-heading">
            <h3 class="panel-title">COURSE OUTCOME ADD</h3>
          </div>
          <div class="panel-body">
		  <div class="split left">
			<div class="centered">
            <!-- <form name="co_add" method="post" action="#">-->
            <fieldset class="form-group">
                <label for="co">CO Code</label>
				<input class="form-control" type="text" name="co_code" id="co_code" placeholder="Eg: CO1" required="required">
			
			</fieldset>
			<fieldset class="form-group">
				<label for="co_name">CO Description</label>
                <textarea class="form-control" name="co_description" id="co_description" style="height:50px;" placeholder="Eg: Apply the steps needed to provide a formal specification for solving the problem" required></textarea>
			</fieldset>
			
              <fieldset class="form-group">
				<label for="select">Select  relative  POs:</label><label id="selectpo">*Mandatory</label>
				
                <div class="row">

                <div class="col-md-3 mt-3" style="margin-top: 15px;">
                        <input type="checkbox" name='check1' value='po1' id="check1" onclick="enableSelect1()" style="margin-right: 10px;">PO1
                            <select name='d1' id='d1' disabled>
                                <option value="0" selected>Select</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                            </select>
                            <input type="hidden" name='d1' id='hidden1' value='0'>

                    </div>
                    <div class="col-md-3 mt-3" style="margin-top: 15px;">
                        <input type="checkbox" name='check2' value='po2' id="check2" onclick="enableSelect2()" style="margin-right: 10px;">PO2
                            <select name='d2' id='d2' disabled>
                                <option value="0" selected>Select</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                            </select>
                            <input type="hidden" name='d2' id='hidden2' value='0'>

                    </div>
                    <div class="col-md-3 mt-3" style="margin-top: 15px;">
                        <input type="checkbox" name='check3' value='po3' id="check3" onclick="enableSelect3()" style="margin-right: 10px;">PO3
                            <select name='d3' id='d3' disabled >
                                <option value="0" selected>Select</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                            </select>
                            <input type="hidden" name='d3' id='hidden3' value='0'>

                    </div>
                    <div class="col-md-3 mt-3" style="margin-top: 15px;">
                        <input type="checkbox" name='check4' value='po4' id="check4" onclick="enableSelect4()" style="margin-right: 10px;">PO4
                            <select name='d4' id='d4' disabled >
                                <option value="0" selected>Select</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                            </select>
                            <input type="hidden" name='d4' id='hidden4' value='0'>
                    </div>
                    <div class="col-md-3 mt-3" style="margin-top: 15px;">
                        <input type="checkbox" name='check5' value='po5' id="check5" onclick="enableSelect5()" style="margin-right: 10px;">PO5
                            <select name='d5' id='d5' disabled >
                                <option value="0" selected>Select</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                            </select>
                            <input type="hidden" name='d5' id='hidden5' value='0'>
                    </div>
                    <div class="col-md-3 mt-3" style="margin-top: 15px;">
                        <input type="checkbox" name='check6' value='po6' id="check6" onclick="enableSelect6()" style="margin-right: 10px;">PO6
                            <select name='d6' id='d6' disabled >
                                <option value="0" selected>Select</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                            </select>
                            <input type="hidden" name='d6' id='hidden6' value='0'>
                    </div>
                    <div class="col-md-3 mt-3" style="margin-top: 15px;">
                        <input type="checkbox" name='check7' value='po7' id="check7"  onclick="enableSelect7()" style="margin-right: 10px;">PO7
                            <select name='d7' id='d7' disabled >
                                <option value="0" selected>Select</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                            </select>
                            <input type="hidden" name='d7' id='hidden7' value='0'>
                    </div>
                    <div class="col-md-3 mt-3" style="margin-top: 15px;">
                        <input type="checkbox" name='check8' value='po8' id="check8" onclick="enableSelect8()" style="margin-right: 10px;">PO8
                            <select name='d8' id='d8' disabled >
                                <option value="0" selected>Select</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                            </select>
                            <input type="hidden" name='d8' id='hidden8' value='0'>
                    </div>
                    <div class="col-md-3 mt-3" style="margin-top: 15px;">
                        <input type="checkbox" name='check9' value='po9' id="check9" onclick="enableSelect9()" style="margin-right: 10px;">PO9
                            <select name='d9' id='d9' disabled >
                                <option value="0" selected>Select</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                            </select>
                            <input type="hidden" name='d9' id='hidden9' value='0'>

                    </div>
                    <div class="col-md-3 mt-3" style="margin-top: 15px;">
                        <input type="checkbox" name='check10' value='po10' id="check10" onclick="enableSelect10()" style="margin-right: 10px;">PO10
                            <select name='d10' id='d10' disabled >
                                <option value="0" selected>Select</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                            </select>
                            <input type="hidden" name='d10' id='hidden10' value='0'>

                    </div>
                    <div class="col-md-3 mt-3" style="margin-top: 15px;">
                        <input type="checkbox" name='check11' value='po11' id="check11"  onclick="enableSelect11()" style="margin-right: 10px;">PO11
                            <select name='d11' id='d11' disabled >
                                <option value="0" selected>Select</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                            </select>
                            <input type="hidden" name='d11' id='hidden11' value='0'>

                    </div>
                    <div class="col-md-3" style="margin-top: 15px;">
                        <input type="checkbox" name='check12' value='po12' id="check12" onclick="enableSelect12()" style="margin-right: 10px;">PO12
                            <select name='d12' id='d12' disabled >
                                <option value="0" selected>Select</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                            </select>
                            <input type="hidden" name='d12' id='hidden12' value='0'>

                    </div>

                    
                </div>
                 
                
                


                </fieldset>
    <div class="button-panel">
		<input type="submit" class="btn btn-info" title="Save" name="submit" value="submit">
        <a href="course_outcome.php"><button type="button" class="btn btn-success">Back</button> </a>
    </div>
    </form>


</div>
</div>
  <div class="split right">
    <div class="centered">
      <br>
      
      <br>

                                    <?php


                                    // $result=mysqli_query($con,"select * from tbl_po");


                                	// 	echo "<table border='1'>
                                	// 	<tr>
                                	// 	<th>PO CODE</th>
                                	// 	<th>PO NAME</th>
                                	// 	<th>PO DESCRIPTION</th>
                                	// 	</tr>";
                                	// 	while($row=mysqli_fetch_array($result))
                                	// 	{
                                	// 		echo "<tr>";
                                	// 		echo "<td>" . $row['po_code'] . "</td>";
                                	// 		echo "<td>" . $row['po_name'] . "</td>";
                                	// 		echo "<td>" . $row['po_description'] . "</td>";
                                	// 		echo "</tr>";
                                	// 	}
                                	// 	     echo "</table>";
                                			?>


                             </td>
                             </tr>
                         </table>
                       </em></p>
                     </div>
                     </div>
</div>

<?php

if(isset($_POST['submit']))
    {
        // values for course_outcome table
        $sub_id = $_GET['subid'];
        echo $sub_id;
        $co_code = $_POST["co_code"];
        $co_description = $_POST["co_description"];
        $status = 1;
        $new_status = '1';


        // values for copo_corelation table
        $d_val1=0;
		$d_val2=0;
		$d_val3=0;
		$d_val4=0;
		$d_val5=0;
		$d_val6=0;
		$d_val7=0;
		$d_val8=0;
        $d_val9=0;
        $d_val10=0;
        $d_val11=0;
        $d_val12=0;

        $d_val1=$_POST['d1'];
		$d_val2=$_POST['d2'];
		$d_val3=$_POST['d3'];
		$d_val4=$_POST['d4'];
		$d_val5=$_POST['d5'];
		$d_val6=$_POST['d6'];
		$d_val7=$_POST['d7'];
		$d_val8=$_POST['d8'];
		$d_val9=$_POST['d9'];
		$d_val10=$_POST['d10'];
		$d_val11=$_POST['d11'];
		$d_val12=$_POST['d12'];

        

        // query to insert data to curse outcome table
        $sql1 = mysql_query( "insert into course_outcome(co_code,subject_id,co_description,status)VALUES ('$co_code','$sub_id','$co_description','$status')",$con);
        
        // query to insert data to copo_corelation table
        $sql2 = mysql_query( "insert into copo_corelation(co_code,po1,po2,po3,po4,po5,po6,po7,po8,po9,po10,po11,po12,status)VALUES ('$co_code','$d_val1','$d_val2','$d_val3','$d_val4','$d_val5','$d_val6','$d_val7','$d_val8','$d_val9','$d_val10','$d_val11','$d_val12','$new_status')",$con);

        if ($sql1) {

            if($sql2){
                echo "<script>alert('Details Added')</script>";
                echo "<script>window.location.href='add_co.php'</script>";
            }
            else{
                echo "<script>alert('Error Occured,Try again')</script>";
                echo "<script>window.location.href='add_co.php'</script>";
            }
           
        } else {
            echo "<script>alert('Error,Try again')</script>";
            echo "<script>window.location.href='add_co.php'</script>";
        }
    }


   	
	?>

  
  </div>
  </div>
  



	<a href="http://localhost:8080/myproject/faculty/dash_home.php"> Back to home</a>

	
  </div>
 </body>
 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</html>
<?php
include("footer.php");
?>