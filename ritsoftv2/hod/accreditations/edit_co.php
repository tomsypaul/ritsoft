<?php
    include('../../connection.php');
	include("../includes/header.php");

    $co_id = $_GET['co_id'];
       
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Edit Course Outcome</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


        <script type="text/javascript" src="js/jquery.min.js"></script>
	
    </head>
    <body>
        <form name="del_co" method="post" action="#">
            <div class="row">
                <div class="col-md-3">

                </div>
                <div class="col-md-8">
                <?php 
                    $sql_co=mysql_query("select co_code from course_outcome where co_id = '$co_id'",$con);
                    while($res_co=mysql_fetch_array($sql_co))
                    {            
                ?>
                    <div class="panel panel-danger" style="margin-top:80px;">
                        <div class="panel-body">
                            <fieldset class="form-group">
                                <label for="course">CO Code</label>
                                <input class="form-control" type="text" name="co_code" id="co_code" value="<?php echo $res_co['co_code']; ?>" required="required" >
            	            </fieldset>
            	        </div>
                        <div class="panel-body">
                            <fieldset class="form-group">
                                <label for="course">CO Description</label>
                                <input class="form-control" type="text" name="co_description" id="co_description" value="<?php echo $res_co['co_description']; ?>" required="required" >
            	            </fieldset>
            	        </div>
            		    <div class="button-panel" style="margin_left:20px; margin_bottom:10px;"> 
            		        <input type="submit" class="btn btn-primary" title="edit" name="edit" id="edit" value="Save Changes">
            			</div>	
            		</div>
                <?php
                    }
                ?>
            	</div>
            </div>
        </form>
    </body>
    <?php
      if(isset($_POST['edit']))
    {
        $co_code = $_POST["co_code"];
        $co_description = $_POST["co_description"];
        
        $update = mysql_query("update course_outcome set co_code='$co_code',co_description='$co_description' WHERE co_id='$co_id'",$con);
        if ($update) {
            echo "<script>alert('Details Updated')</script>";
            echo "<script>window.location.href='course_outcome.php'</script>";
        } else {
            echo "<script>alert('Failed to Update')</script>";
            echo "<script>window.location.href='course_outcome.php'</script>";

        }
    } 
    
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</html>

