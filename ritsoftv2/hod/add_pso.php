<?php
include("includes/header.php");
include("includes/sidenav.php");
include("includes/connection1.php");
$dept_name = $_GET['dept_name'];


?>


<div id="page-wrapper">

	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-8">
				<h1 class="page-header">View Program Specific Outcome</h1>
			</div>
			<div class="col-lg-4 text-right">
				<button class="btn btn-primary" style="margin-top: 50px; font-size:large"  data-toggle="modal" data-target="#addCO_modal">
					<span style="margin-right: 10px;"><i class="fa fa-plus"></i></span>Add PSO
				</button>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="addCO_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="exampleModalLongTitle">Add Program Specific Outcome</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="">
                                <div class="modal-body">
                                    <fieldset class="form-group">
                                        <label for="po">PSO Code</label>
			                        	 <input class="form-control" type="text" name="pso_code" id="pso_code" value="" placeholder="Eg: PSO1" required="required" style="text-transform: uppercase">          
			                        </fieldset>
                                    <fieldset class="form-group">
                                        <label for="po">PSO Title</label>
			                        	<input class="form-control" type="text" name="pso_title" id="pso_title" value="" placeholder="" required="required" style="text-transform: uppercase">         
			                        </fieldset>
			                        <fieldset class="form-group">
			                        	<label for="po_description">PSO Description</label>
                                        <textarea class="form-control" name="pso_description" id="pso_description" style="height:50px;" required style="text-transform: capitalize"></textarea>
			                        </fieldset>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary" name="add_pso" id="add_pso">Save changes</button>
                                </div>
                            </form>
                            <?php
                                if(isset($_POST['add_pso']))
                                {
                                    $pso_code = strtoupper($_POST["pso_code"]);
                                    $pso_title = strtoupper($_POST["pso_title"]);
                                    $pso_description = ucfirst($_POST["pso_description"]);
									$status = 1;
									$sql = mysql_query( "insert into program_specific_outcome(pso_code,pso_title,pso_description,dept_name,pso_status)VALUES ('$pso_code','$pso_title','$pso_description','$dept_name','$status')",$con);
                                    if ($sql) {
                                        echo "<script>alert('Succesfully Added')</script>";
                                        echo "<script>window.location.href='add_pso.php?dept_name=$dept_name'</script>";
                                    } else {
                                        echo "<script>alert('Failed to Add')</script>";
                                        echo "<script>window.location.href='add_pso.php?dept_name=$dept_name'</script>";
                                    
                                    }
                                } 
                            
                            ?>
                            
                        </div>
                    </div>
                </div>

		<?php
			$pso_count_sel = mysql_query("select * from program_specific_outcome where dept_name = '$dept_name'",$con);

			$pso_count = mysql_num_rows($pso_count_sel);

			if($pso_count == 0)
			{
		?>
		<div class="text-center">
				<h3>No Program Specific Outcome Availabe</h3>

		</div>
		<?php
			}
			else{
		?>

        <div class="card">
			<div class="card-body">
				<table class="table table-success table-bordered table-striped">
					<thead style="font-size:20px;">
						<tr>
							<th style="text-align: center;" > PSO </th>
                            <th style="text-align: center;" > PSO Title </th>
							<th style="text-align: center;" > PSO Description </th>
							<th style="text-align: center;" > Action </th>
						</tr>
					</thead>
					<tbody>
						<?php

                            // $dept_name = $_GET['dept_name'];
							
							$sql_pso = mysql_query("select * from program_specific_outcome where dept_name = '$dept_name'",$con);
							while($res_pso = mysql_fetch_array($sql_pso))
    						{
							
					    ?>
							<tr style="font-size:18px;"> 
		    	    		    <td style="text-align: center; "><?php echo $res_pso['pso_code']; ?> </td>
                                <td style="text-align: center; "><?php echo $res_pso['pso_title']; ?> </td>

		    	    		    <td style="text-align: center;"><?php echo $res_pso['pso_description']; ?></td>

		    	    		    <td style="text-align: center;">
                                    <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#<?php echo $res_pso['pso_id'] ?>ModalCenter">
										<i class="fa fa-pencil" aria-hidden="true" style="font-size: 18px"></i>
									</button>
									<a href="accreditation/delete_pso.php?pso_id=<?php echo $res_pso['pso_id']; ?>&dept_name=<?php echo $dept_name; ?>">
										<button type="button" class="btn btn-danger">
											<i class="fa fa-trash" aria-hidden="true" style="font-size: 18px"></i>
										</button>
							        </a>
									
								</td>

							</tr>
                            <!-- Modal -->
                            <div class="modal fade" id="<?php echo $res_pso['pso_id'] ?>ModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="exampleModalLongTitle">Edit Program Specific Outcome</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="">
                                            <div class="modal-body">
                                                <fieldset class="form-group">
                                                    <label for="co">PSO Code</label>
			                                    	<input class="form-control" type="text" name="pso_code" id="pso_code"  value="<?php echo $res_pso['pso_code']; ?> " placeholder="Eg: PSO1" required="required" style="text-transform: uppercase">         
			                                    </fieldset>
                                                <fieldset class="form-group">
                                                    <label for="co">PSO Title</label>
			                                    	<input class="form-control" type="text" name="pso_title" id="pso_title" value="<?php echo $res_pso['pso_title']; ?> " placeholder="" required="required" style="text-transform: uppercase">         
			                                    </fieldset>
			                                    <fieldset class="form-group">
			                                    	<label for="co_name">PSO Description</label>
                                                    <textarea class="form-control" name="pso_description" id="pso_description" style="height:50px;" required><?php echo $res_pso['pso_description']; ?></textarea>
			                                    </fieldset>
                                            </div>
                                            <input type="hidden" name="pso_id" value="<?php echo $res_pso['pso_id']; ?>">
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" name="submit" id="submit">Save changes</button>
                                            </div>
                                        </form>
                                        <?php
                                            if(isset($_POST['submit']))
                                            {
                                                $pso_id = $_POST['pso_id'];
                                                $pso_code = strtoupper($_POST["pso_code"]);
                                                $pso_title = strtoupper( $_POST["pso_title"]);
                                                $pso_description = $_POST["pso_description"];


                                                $update = mysql_query("update program_specific_outcome set pso_code='$pso_code', pso_title='$pso_title',pso_description='$pso_description' WHERE pso_id='$pso_id'",$con);
                                                if ($update) {
                                                    echo "<script>alert('Succesfully Updated')</script>";
                                                    echo "<script>window.location.href='add_pso.php?dept_name=$dept_name'</script>";
                                                } else {
                                                    echo "<script>alert('Failed to Update')</script>";
                                                    echo "<script>window.location.href='add_pso.php?dept_name=$dept_name'</script>";
                                                
                                                }
                                            } 
                                        
                                        ?>
                                        
                                    </div>
                                </div>
                            </div>
						<?php
				    	    }
				    	?>
					</tbody>
				</table>
				
			</div>
			<?php 
				}			
			?>
		</div>
        
					
<?php include("includes/footer.php");?>
