<?php
include("includes/header.php");
include("includes/sidenav.php");
include("includes/connection1.php");
$sub_id = $_GET['subid'];


?>


<div id="page-wrapper">

	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-8">
				<h1 class="page-header">View Course Outcome</h1>
			</div>
			<div class="col-lg-4 text-right">
				<button class="btn btn-primary" style="margin-top: 50px; font-size:large"  data-toggle="modal" data-target="#addCO_modal">
					<span style="margin-right: 10px;"><i class="fa fa-plus"></i></span>Add CO
				</button>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="addCO_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="exampleModalLongTitle">Add Course Outcome</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="">
                                <div class="modal-body">
                                    <fieldset class="form-group">
                                        <label for="co">CO Code</label>
			                        	<input class="form-control" type="text" name="co_code" id="co_code" value="" placeholder="Eg: CO1" required="required" style = "text-transform:uppercase;">         
			                        </fieldset>
			                        <fieldset class="form-group">
			                        	<label for="co_name">CO Description</label>
                                        <textarea class="form-control" name="co_description" id="co_description" style="height:50px;" required style="text-transform: capitalize"></textarea>
			                        </fieldset>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary" name="add_co" id="add_co">Save changes</button>
                                </div>
                            </form>
                            <?php
                                if(isset($_POST['add_co']))
                                {
                                    $co_code = strtoupper($_POST["co_code"]);
                                    $co_description = ucfirst($_POST["co_description"]);
									$status = 1;
									$sql = mysql_query( "insert into course_outcome(co_code,subject_id,co_description,status)VALUES ('$co_code','$sub_id','$co_description','$status')",$con);
                                    if ($sql) {
                                        echo "<script>alert('Succesfully Added')</script>";
                                        echo "<script>window.location.href='co_view.php?subid=$sub_id'</script>";
                                    } else {
                                        echo "<script>alert('Failed to Update')</script>";
                                        echo "<script>window.location.href='co_view.php?subid=$sub_id'</script>";
                                    
                                    }
                                } 
                            
                            ?>
                            
                        </div>
                    </div>
                </div>

		<?php
			$co_count_sel = mysql_query("select * from course_outcome where subject_id = '$sub_id'",$con);

			$co_count = mysql_num_rows($co_count_sel);

			if($co_count == 0)
			{
		?>
		<div class="text-center">
				<h3>No Course Outcome Availabe</h3>

		</div>
		<?php
			}
			else{
		?>

        <div class="card">
			<div class="card-body">
				<table class="table table-success table-bordered table-striped">
					<thead style="font-size:20px;">
						<tr>
							<th style="text-align: center;" > CO </th>
							<th style="text-align: center;" > CO Description </th>
							<th style="text-align: center;" > Action </th>
						</tr>
					</thead>
					<tbody>
						<?php

                            // $sub_id = $_GET['subid'];
							
							$sql_co=mysql_query("select * from course_outcome where subject_id = '$sub_id'",$con);
							while($res_co=mysql_fetch_array($sql_co))
    						{
							
					    ?>
							<tr style="font-size:18px;"> 
		    	    		    <td style="text-align: center; "><?php echo $res_co['co_code']; ?> </td>

		    	    		    <td style="text-align: center;"><?php echo $res_co['co_description']; ?></td>

		    	    		    <td style="text-align: center;">
								<!-- <a href="edit_co.php?co_id=<?php //echo $res_co['co_id'] ?>"> -->
									<!-- <button type="button" class="btn btn-info"  onclick="editmodal()"> -->
                                    <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#<?php echo $res_co['co_id'] ?>ModalCenter">
										<i class="fa fa-pencil" aria-hidden="true" style="font-size: 18px"></i>
									</button>
							    <!-- </a> -->
									<a href="delete_co.php?co_id=<?php echo $res_co['co_id'] ?>&sub_id=<?php echo $sub_id;?>">
										<button type="button" class="btn btn-danger">
											<i class="fa fa-trash" aria-hidden="true" style="font-size: 18px"></i>
										</button>
							        </a>
									
								</td>

							</tr>
                            <!-- Modal -->
                            <div class="modal fade" id="<?php echo $res_co['co_id'] ?>ModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="exampleModalLongTitle">Edit Course Outcome</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="">
                                            <div class="modal-body">
                                                <fieldset class="form-group">
                                                    <label for="co">CO Code</label>
			                                    	<input class="form-control" type="text" name="co_code" id="co_code" value="<?php echo $res_co['co_code']; ?> " placeholder="Eg: CO1" required="required">         
			                                    </fieldset>
			                                    <fieldset class="form-group">
			                                    	<label for="co_name">CO Description</label>
                                                    <textarea class="form-control" name="co_description" id="co_description" style="height:50px;" required><?php echo $res_co['co_description']; ?></textarea>
			                                    </fieldset>
                                            </div>
                                            <input type="hidden" name="co_id" value="<?php echo $res_co['co_id']; ?>">
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" name="submit" id="submit">Save changes</button>
                                            </div>
                                        </form>
                                        <?php
                                            if(isset($_POST['submit']))
                                            {
                                                $co_id = $_POST['co_id'];
                                                $co_code = $_POST["co_code"];
                                                $co_description = $_POST["co_description"];


                                                $update = mysql_query("update course_outcome set co_code='$co_code',co_description='$co_description' WHERE co_id='$co_id'",$con);
                                                if ($update) {
                                                    echo "<script>alert('Succesfully Updated')</script>";
                                                    echo "<script>window.location.href='co_view.php?subid=$sub_id'</script>";
                                                } else {
                                                    echo "<script>alert('Failed to Update')</script>";
                                                    echo "<script>window.location.href='co_view.php?subid=$sub_id'</script>";
                                                
                                                }
                                            } 
                                        
                                        ?>
                                        
                                    </div>
                                </div>
                            </div>
						<?php
				    	    }
				    	?>
					</tbody>
				</table>
				
			</div>
			<?php 
				}			
			?>
		</div>
        
					
<?php include("includes/footer.php");?>
