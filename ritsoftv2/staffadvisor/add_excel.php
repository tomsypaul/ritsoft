<?php
/**
 * @Author: indran
 * @Date:   2018-08-07 14:18:17
 * @Last Modified by:   indran
 * @Last Modified time: 2018-08-07 14:42:04
 */
session_start();

$class_id = $_SESSION['classid'];


if(isset($_POST['excel']))  {
 // $reli=$_POST['religion'];

 // $search_query=  explode("/", $reli);

  $link = mysqli_connect("127.0.0.1","ritsoftv2","ritsoftv2", "ritsoftv2");
  $sql = "SELECT * FROM count_feedback WHERE classid='$class_id' ";


  $result = mysqli_query($link, $sql);


  require('PHPExcel.php');

// create new PHPExcel object
  $objPHPExcel = new PHPExcel;

// set default font
  $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');

// set default font size
  $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);

// create the writer
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");


// currency format, € with < 0 being in red color
  $currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';

// number format, with thousands separator and two decimal points.
  $numberFormat = '#,#0.##;[Red]-#,#0.##';



// writer already created the first sheet for us, let's get it
  $objSheet = $objPHPExcel->getActiveSheet();

// rename the sheet
  $objSheet->setTitle('Students List');


   $objSheet->getStyle('A1:AE1')->getFont()->setBold(true)->setSize(15);

  $objSheet->getCell('A1')->setValue('Questions');
  $objSheet->getCell('B1')->setValue('1');
  $objSheet->getCell('C1')->setValue('2');
  $objSheet->getCell('D1')->setValue('3');
  $objSheet->getCell('E1')->setValue('4');
  $objSheet->getCell('B2')->setValue('Yes');
  $objSheet->getCell('C2')->setValue('No');
  $objSheet->getCell('D2')->setValue('Not sure');
  $objSheet->getCell('B4')->setValue('Yes');
  $objSheet->getCell('C4')->setValue('No');
  $objSheet->getCell('B6')->setValue('1-5');
  $objSheet->getCell('C6')->setValue('6-10');
  $objSheet->getCell('D6')->setValue('11-15');
  $objSheet->getCell('E6')->setValue('Above 15');
  $objSheet->getCell('B8')->setValue('By Watching recorded class');
  $objSheet->getCell('C8')->setValue('With the help of notes shared by the teacher ');
  $objSheet->getCell('D8')->setValue('self study');
  $objSheet->getCell('B10')->setValue('Yes');
  $objSheet->getCell('C10')->setValue('No');
  $objSheet->getCell('D10')->setValue('Sometimes');
  $objSheet->getCell('A2')->setValue('Was the course objectives made clear by the teacher during the commencement of the course?');
  $objSheet->getCell('A4')->setValue('Were there any classes you missed due to network issues?');
  $objSheet->getCell('A6')->setValue('If so how many?');
  $objSheet->getCell('A8')->setValue('How did you manage to make up the loss?');
  $objSheet->getCell('A10')->setValue('Did the teacher dictate notes online and made you write down the notes?');



   $i=3;
 while($row = mysqli_fetch_array($result)){

    $objSheet->getCell('B'.$i)->setValue($row['op1']);
    $objSheet->getCell('C'.$i)->setValue($row['op2']);
    $objSheet->getCell('D'.$i)->setValue($row['op3']);
    $objSheet->getCell('E'.$i)->setValue($row['op4']);
    $i=$i+2;
  }


  $objSheet->getColumnDimension('A')->setAutoSize(true);
  $objSheet->getColumnDimension('B')->setAutoSize(true);
  $objSheet->getColumnDimension('C')->setAutoSize(true);
  $objSheet->getColumnDimension('D')->setAutoSize(true);
  $objSheet->getColumnDimension('E')->setAutoSize(true);
 


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="file.xlsx"');
header('Cache-Control: max-age=0');

  $objWriter->save('php://output');
  exit;

}

$_SESSION['alert']=$alert;
if(isset($_SERVER['HTTP_REFERER']))
{
  header('Location: ' . $_SERVER['HTTP_REFERER']);   
}
else 
{
  header('Location:loginpage.php');     
}

?>